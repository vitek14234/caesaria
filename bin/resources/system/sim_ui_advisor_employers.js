sim.ui.advisors.hirepriority.show = function(type, priority) {
  var parlor = g_ui.find("ParlorWindow");

  var resolution = g_session.resolution;
  var w = new Window(parlor);
  w.name = "#hirepriority";
  w.geometry = {x:0, y:0, w:416, h:144};
  w.title = _u("priority_level");
  w.closeAfterKey( {escape:true,rmb:true} );
  w.setProperty("last", priority);
  w.setProperty("type", type);
  w.setProperty("priority", priority);

  var lbExit = w.addLabel(0, w.h - 30, w.w, 20);
  lbExit.text = _u("right_click_to_exit");
  lbExit.font = "FONT_1";
  lbExit.textAlign = { v:"center", h:"center" };

  w.update = function(i)
  {
    w.setProperty("priority", i+1);
    for (var tb=0; tb < w.buttons.length; tb++)
      w.buttons[tb].pressed = tb==i;
  }

  /*w.keyPressedCallback = function(event)
  {

  }*/

  w.addHrButton = function(x,y,i)
  {
    var btn = w.addButton(x, y, 28, 28);
    btn.text = "" + (k+1);
    btn.style = "flatBorderLine";
    btn.bistate = true;
    btn.pressed = priority > 0 ? i+1 == priority : false;
    btn.tooltip = _u("priority_button_tolltip");
    btn.callback = function() { w.update(i); };

    w.buttons.push(btn);
  }

  var noPr = w.addButton(68, 78, 296, 26);
  noPr.text = _u("no_priority");
  noPr.style = "flatBorderLine";
  noPr.pressed = priority == 0;
  noPr.callback = function() { w.update(0); }
  w.buttons.push(noPr);

  var start = {x:65, y:44};
  for (var k=0; k < 9; k++)
  {
    w.addHrButton(start.x, start.y,k);
    start.x += btn.w+5;
  }

  w.moveToCenter();
  w.mayMove = false;
  w.setModal();
}
